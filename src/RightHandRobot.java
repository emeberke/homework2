/**
 * Emelie Gage
 * Right hand robot only moves to the right until it runs into a wall
 * inherits from robot
 */
public class RightHandRobot extends Robot{
    private int direction;
    public RightHandRobot (Maze maze) {
        super(maze);
        direction = 2;
    }
    /**
     * choose move direction takes int direction and cycles through possible moves until a valid
     * move is found
     * @return direction that is the first open move to the right
     */
    public int chooseMoveDirection() {
        if(direction==0) {
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                direction = 1;
                return 1;
            }
            if(maze.openCell(super.getCurrentRow()-1, super.getCurrentCol())) {
                direction = 0;
                return 0; }
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()-1)) {
                direction = 3;
                return 3;
            }
            if(maze.openCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                direction = 2;
                return 2;
            }
        }
        if(direction ==1){
            if(maze.openCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                direction = 2;
                return 2;
            }
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                direction = 1;
                return 1;
            }
            if(maze.openCell(super.getCurrentRow()-1,super.getCurrentCol())) {
                direction = 0;
                return 0;
            }
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()-1)) {
                direction = 3;
                return 3;
            }
        }
        if(direction==2){
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()-1)) {
                direction = 3;
                return 3;
            }
            if(maze.openCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                direction = 2;
                return 2;
            }
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                direction = 1;
                return 1;
            }
            if(maze.openCell(super.getCurrentRow()-1,super.getCurrentCol())) {
                direction = 0;
                return 0;
            }
        }
        if(direction==3){
            if(maze.openCell(super.getCurrentRow()-1,super.getCurrentCol())) {
                direction = 0;
                return 0;
            }
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()-1)){
                direction = 3;
                return 3; }
            if(maze.openCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                direction = 2;
                return 2;
            }
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                direction = 1;
                return 1;
            }
        }
        return direction;
    }
    /**
     * move method takes the direction returned and moves the robot accordingly
     * @param direction direction passed to move
     * @return flag that determines whether or not the move is valid
     */
    public boolean move(int direction) {
        boolean flag=false;
        char clear = ' ';
        if(direction ==0) {
            if(maze.openCell(super.getCurrentRow()-1,super.getCurrentCol())) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentRow(super.getCurrentRow()-1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==1) {
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentCol(super.getCurrentCol()+1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==2) {
            if(maze.openCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentRow(super.getCurrentRow()+1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==3){
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()-1)) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentCol(super.getCurrentCol()-1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        else {
            flag = false;
        }
        return flag;
    }
}
