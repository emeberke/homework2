import java.io.*;
import java.util.Scanner;
/**
 * Emelie Gage
 * 1) C
 * 2) A
 * 3) A
 * 4) A
 * 5) A
 * 6) B
 * 7) A
 * 8) C
 * 9) D
 * 10) D
 * 11) A
 * 12) A
 * 13) A
 * 14) B
 * 15) A
 * 16) A
 * 17) C
 * 18) see problem18
 */
public class Maze {
    private int row, col, startRow, startCol, exitRow, exitCol;
    private char maze[][];
    public Maze(File filename) throws IOException {
        Scanner readFile = new Scanner(filename);
        row = readFile.nextInt();
        col = readFile.nextInt();
        startRow = readFile.nextInt();
        startCol = readFile.nextInt();
        exitRow = readFile.nextInt();
        exitCol = readFile.nextInt();
        maze = new char[getRows()][getCols()];
        readFile.nextLine();
        for(int i=0; i<row;i++){
            String file=readFile.nextLine();
            for(int j=0; j<col;j++){
                maze[i][j]=file.charAt(j);
            }
        }
    }
    /**
     * Get rows returns number rows
     * @return row
     */
    public int getRows() {
        return row;
    }
    /**
     * Get cols returns number cols
     * @return col
     */
    public int getCols() {
        return col;
    }
    /**
     * Get start row gives the robot a place to start
     * @return starting row
     */
    public int getStartRow() {
        return startRow;
    }
    /**
     * Get start col gives robot a place to start
     * @return starting col
     */
    public int getStartCol() {
        return startCol;
    }
    /**
     * Get exit row assigns row to exit
     * @return exit row
     */
    public int getExitRow() {
        return exitRow;
    }
    /**
     * Get exit col assigns col to exit
     * @return exit col
     */
    public int getExitCol() {
        return exitCol;
    }
    /**
     * get cell returns cell currently occupied
     * @param row location
     * @param col location
     * @return maze at row/col of location
     */
    public char getCell(int row, int col) {
       return maze[row][col];
    }
    /**
     * Opencell determines if the cell is open
     * @param row row of cell to check
     * @param col col of cell to check
     * @return flag false if cell is not open, true if it is
     */
    public boolean openCell(int row, int col) {
        if(row<0||row>=this.row)
            return false;
        if(col<0||col>=this.col)
            return false;
        if(maze[row][col]=='*')
           return false;
        if(maze[row][col]==' '||maze[row][col]=='S')
            return true;
        if(maze[row][col]=='X')
            return true;
        return false;
    }

    /**
     * Pac cell for the pac man robot
     * @param row row to check
     * @param col col to check
     * @return flag false if robot tries to move out of bounds
     */
    public boolean pacCell(int row, int col) {
        if(row<0||row>=this.row)
            return false;
        if(col<0||col>=this.col)
            return false;
        if(maze[row][col]=='*')
            return true;
        if(maze[row][col]=='X')
            return true;
        if(maze[row][col]==' '||maze[row][col]=='S')
            return true;
        return false;
    }
    /**
     * set cell sets the cell for the robot to move into
     * @param row row of cell
     * @param col col of cell
     * @param newCh this character holds the value we will put in
     */
    public void setCell(int row, int col, char newCh) {
        maze[row][col]=newCh;
    }
    /**
     * to string will print the maze
     * @return maze as a string output
     */
    public String toString() {
        String output = "";
        for (int i = 0; i<maze.length; i++){
            for(int j = 0; j<maze[i].length; j++) {
                output += maze[i][j] +" ";
            }
            output +="\n";
        }
        return output;
    }
}
