import java.util.*;
/**
 * Emelie Gage
 * Random robot inherits from robot
 * takes the maze from the constructor
 */
public class RandomRobot extends Robot{
    private int direction;
    public RandomRobot (Maze maze){
        super(maze);
        direction = chooseMoveDirection();
       // maze.setCell(maze.getStartRow(), maze.getStartCol(), super.getRobotName());
    }
    /**
     * choose move direction generates a random int to be returned
     * @return direction, a randomly generated integer
     */
    public int chooseMoveDirection() {
        Random rand = new Random();
        direction = rand.nextInt(4);
        return direction;
    }
    /**
     * move method will take the direction passed and move the robot accordingly
     * @param direction direction passed to move
     * @return boolean flag determines whether or not the move is valid
     */
    public boolean move(int direction) {
        boolean flag=false;
        char clear = ' ';
        if(direction ==0) {
            if(maze.openCell(super.getCurrentRow()-1,super.getCurrentCol())) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentRow(super.getCurrentRow()-1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==1) {
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentCol(super.getCurrentCol()+1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==2) {
            if(maze.openCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentRow(super.getCurrentRow()+1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==3){
            if(maze.openCell(super.getCurrentRow(),super.getCurrentCol()-1)) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentCol(super.getCurrentCol()-1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        else {
            flag = false;
        }
        return flag;
    }
}
