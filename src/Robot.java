/**
 * Emelie Gage
 */
public abstract class Robot {
    private int currentRow, currentCol;
    private char robotName;
    protected Maze maze;
    /**
     * default constructor
     */
    public Robot (){
    }
    /**
     * Robot constructor
     * @param maze passes maze to the constructor
     */
    public Robot (Maze maze) {
        this.maze = maze;
        currentRow = maze.getStartRow();
        currentCol = maze.getStartCol();
        robotName = 'R';
    }
    /**
     * abstract method will be overridden in subclasses
     * @return move direction
     */
    public abstract int chooseMoveDirection();
    /**
     * abstract method will be overridden in subclasses
     * @param direction direction passed to move
     * @return true or false if runs into a wall
     */
    public abstract boolean move(int direction);
    /**
     * Solved method will return true when exit is reached
     * @return boolean flag true/false
     */
    public boolean solved () {
        boolean flag = false;
        if(currentRow==maze.getExitRow()&&currentCol==maze.getExitCol())
            flag = true;
        return flag;
    }
    /**
     * set current row for robot
     * @param row current row
     */
    public void setCurrentRow(int row){
        currentRow = row;
    }
    /**
     * get current row
     * @return row
     */
    public int getCurrentRow(){
        return currentRow;
    }
    /**
     * set current col for robot
     * @param col current col
     */
    public void setCurrentCol(int col){
        currentCol = col;
    }
    /**
     * get current col
     * @return col
     */
    public int getCurrentCol(){
        return currentCol;
    }
    /**
     * get robot name from code
     * @return char of robot name
     */
    public char getRobotName(){
        return robotName;
    }
}
