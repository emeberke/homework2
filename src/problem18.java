public class problem18 {
    public abstract class Vehicle { //super class
        public abstract double getMilesPerGallon();
        //Other methods...
    }
    public class Car extends Vehicle {//sub class
        private double mpg;
        public double getMilesPerGallon()
        {
            return mpg;
        }
        //Other methods...
    }
}
