import java.util.Random;

/**
 * This robot can eat the walls, effectively going wherever it wants
 * Emelie Gage
 */
public class PacMan extends Robot{
    private int direction;
    public PacMan (Maze maze){
        super(maze);
        direction = chooseMoveDirection();
    }

    /**
     * Choose move direction just like random robot
     * @return direction to travel
     */
    public int chooseMoveDirection() {
        Random rand = new Random();
        direction = rand.nextInt(4);
        return direction;
    }

    /**
     * The robot can move using pac cell which allows it to eat walls
     * @param direction direction passed to move
     * @return true for valid move
     */
    public boolean move(int direction) {
        boolean flag=false;
        char clear = ' ';
        if(direction ==0) {
            if(maze.pacCell(super.getCurrentRow()-1,super.getCurrentCol())) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentRow(super.getCurrentRow()-1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==1) {
            if(maze.pacCell(super.getCurrentRow(),super.getCurrentCol()+1)) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentCol(super.getCurrentCol()+1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==2) {
            if(maze.pacCell(super.getCurrentRow()+1,super.getCurrentCol())) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentRow(super.getCurrentRow()+1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        if(direction ==3){
            if(maze.pacCell(super.getCurrentRow(),super.getCurrentCol()-1)) {
                flag = true;
                maze.setCell(super.getCurrentRow(), super.getCurrentCol(),clear);
                setCurrentCol(super.getCurrentCol()-1);
                maze.setCell(super.getCurrentRow(),super.getCurrentCol(), super.getRobotName());
            }
        }
        else {
            flag = false;
        }
        return flag;
    }
}
